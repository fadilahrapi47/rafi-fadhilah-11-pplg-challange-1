@extends('dashboard.layouts.main')

@section('konten')

<div class="container">
    <div class="row my-3">
        <div class="col-lg-8">
            <h2 class="mb-3">{{ $post->judul }}</h2>
                <div class="d-flex my-3">
                    <a href="/dashboard/posts/{{ $post->slug }}/edit" class="btn btn-warning"><span data-feather="edit"></span>Edit</a>
                    <form action="/dashboard/posts/{{ $post->slug }}" method="post" class="d-inline">
                        @method('delete') 
                        @csrf
                        <button class="btn btn-danger" onclick="return confirm('You want to delete this post?')"><span data-feather="x-circle"></span>Delete</button>
                    </form>
                </div>
            <h5>{{ $post["surah"] }}</h5>
            
                @if ($post->image)
                    <img src="{{ asset('storage/' . $post->image) }}" alt="..." class="img-fluid">
                @else
                    <img src="{{ url('Hoki128.png') }}" alt="..." class="img-fluid">
                @endif

            <article class="my-4 fs-5">
            {!! $post->isi !!}
            </article>

            <a href="/dashboard/posts" class="btn btn-success mb-4"><span data-feather="arrow-left"></span>Back to My Post</a>
        </div>
    </div>
</div>

@endsection