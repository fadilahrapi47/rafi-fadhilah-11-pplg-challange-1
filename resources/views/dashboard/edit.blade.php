@extends('dashboard.layouts.main')

@section('konten')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit Postingan</h1>
    </div>

    <div class="col-lg-10">
        <form action="/dashboard/posts/{{ $post->slug }}" method="post" class="mb-5" enctype="multipart/form-data"> 
            @method('put')
            @csrf
            <div class="mb-3">
              <label for="judul" class="form-label">Judul</label>
              <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" required autofocus value="{{ old('judul', $post->judul) }}">
              @error('judul') <div class="invalid-feedback">Judul ini harus di isi!</div> @enderror
            </div>
            <div class="mb-3">
              <label for="slug" class="form-label">Slug</label>
              <input type="text" class="form-control @error('slug') is-invalid @enderror" id="slug" name="slug" required value="{{ old('slug', $post->slug) }}">
            </div>
            <div class="mb-3">
              <label for="category" class="form-label">Kategori</label>
              <select class="form-select" name="category_id">
                @foreach ($categories as $category)
                  @if(old('category_id', $post->category_id) == $category->id)
                    <option value="{{ $category->id }}" selected>{{ $category->nama }}</option>
                  @else
                    <option value="{{ $category->id }}">{{ $category->nama }}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="mb-3">
              <label for="image" class="form-label">Masukkan Gambar</label>
              <input class="form-control @error('image') is-invalid @enderror" type="file" id="image" name="image" onchange="previewImage()">
              <input type="hidden" name="gambarlama" value="{{ $post->image }}">
              @error('image') <div class="invalid-feedback">{{ $message }}</div> @enderror
              @if($post->image)
                <img src="{{ asset('storage/' . $post->image) }}" class="img-preview img-fluid mt-3 col-sm-3 d-block">          
              @else
                <img class="img-preview img-fluid mt-3 col-sm-3">      
              @endif
            </div>
            <div class="mb-3">
              <label for="isi" class="form-label">Body</label>
              @error('isi') <p class="text-danger">Isilah tulisan ini!</p> @enderror
              <input id="isi" type="hidden" name="isi" value="{{ old('isi', $post->isi) }}">
              <trix-editor input="isi"></trix-editor>
            </div>

              <button type="submit" class="btn btn-primary">Update post</button>
        </form>
    </div>

    <script>
        const judul = document.querySelector('#judul');
        const slug = document.querySelector('#slug');

        judul.addEventListener('change', function() {
            fetch('/dashboard/posts/checkSlug?judul=' + judul.value)
                .then(response => response.json())
                .then(data => slug.value = data.slug)
        });

        document.addEventListener('trix-file-accept', function(e) {
          e.preventDafault();
        })

        function previewImage() {
          const image = document.querySelector('#image');
          const imgPreview = document.querySelector('.img-preview');

          imgPreview.style.display = 'block';

          const oFReader = new FileReader();
          oFReader.readAsDataURL(image.files[0]);

          oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
          }
        }
    </script> 

@endsection