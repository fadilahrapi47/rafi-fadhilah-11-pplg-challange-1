@extends('layouts.main')

@section('views')

<div class="row justify-content-center">
    <div class="col-md-5 mt-5">

        @if(session()->has('sukses'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong> {{ session('sukses') }} </strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif

        @if(session()->has('gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong> {{ session('gagal') }} </strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif

        <main class="form-signin w-100 m-auto">
            <h1 class="h3 mb-3 fw-normal text-center">Please login</h1>
            <form method="post" action="/login">
              @csrf
              <div class="form-floating">
                <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="username" autofocus required>
                <label for="username">Username</label>
                @error('username')
                  <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-floating">
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="password" required>
                <label for="password">Password</label>
                @error('password')
                  <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
              <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>
            </form>
            <small class="d-block mt-3 text-center">Belum punya akun? <a href="/register">Buat Akun disini!</a></small>
        </main>
    </div>
</div>

@endsection