<div class="card mb-3">
  @if ($posts[0]->image)
    <a href="/post/{{ $posts[0]->slug }}"><img src="{{ asset('storage/' . $posts[0]->image) }}" alt="..." class="img-fluid"></a>
  @else
    <img src="{{ url('Hoki128.png') }}" class="card-img-top" alt="...">
  @endif
    <div class="card-body text-center">
      <a href="/post/{{ $posts[0]->slug }}" class="text-decoration-none text-dark"><h3 class="card-title">{{ $posts[0]->judul }}</h3>
      <p>
        <small class="text-muted">
        Ditulis oleh <a href="/posts?author={{ $posts[0]->author->username }}" class="text-decoration-none">{{ $posts[0]->author->name }}</a> 
        dalam kaetegori <a href="/posts?category={{ $posts[0]->category->slug }}" class="text-decoration-none">{{ $posts[0]->category->nama }}</a>
        {{ $posts[0]->created_at->diffForHumans() }}
      </small>
    </p>

      <p class="card-text">{{ $posts[0]->excrept }}</p>
      <a href="/post/{{ $posts[0]->slug }}" class="text-decoration-none btn btn-primary">Read More</a>
     
    </div>
  </div>