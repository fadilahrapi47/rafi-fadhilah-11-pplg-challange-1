<div class="container">
    <div class="row">
        @foreach ($posts->skip(1) as $post) 
        <div class="col-md-4 mb-4">
            <div class="card">
              <div class="position-absolute px-3 py-2" style="background-color: rgba(0, 0, 0, 0.6)"><a href="/posts?category={{ $post->category->slug }}" class="text-white text-decoration-none">{{ $post->category->nama }}</a></div>
              @if ($post->image)
                <img src="{{ asset('storage/' . $post->image) }}" alt="..." class="img-fluid">
              @else
                <img src="{{ url('Error.png') }}" class="card-img-top" alt="...">
              @endif  
                <div class="card-body">
                  <h5 class="card-title">{{ $post->judul }}</h5>
                  <p>
                    <small class="text-muted">
                    Ditulis oleh <a href="/posts?author={{ $post->author->username }}" class="text-decoration-none">{{ $post->author->name }}</a> 
                    {{ $post->created_at->diffForHumans() }}
                    </small>
                  </p>
                  <p class="card-text">{{ $post->excrept }}</p>
                  <a href="/post/{{ $post->slug }}" class="btn btn-primary">Read More</a>
                </div>
              </div>
        </div>
        @endforeach
    </div>
</div>