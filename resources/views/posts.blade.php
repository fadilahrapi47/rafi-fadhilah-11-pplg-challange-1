@extends('layouts.main')

@section('views')

    <h1 class="mb-3 text-center">{{ $title }}</h1>

    @include('parts.search')
    
    @if ($posts->count())

    @include('parts.overlay')
    
    @include('parts.cards')

    @else
    <p class="text-center fs-4">No post found.</p>
    @endif

    <div class="d-flex justify-content-center">
    {{ $posts->links() }}
    </div>

@endsection