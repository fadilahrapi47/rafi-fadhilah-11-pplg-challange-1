@extends('layouts.main')

@section('views')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>{{ $post->judul }}</h2>
                <p class="mb-3 mt-5">
                    Ditulis oleh <a href="/posts?author={{ $post->author->username }}" class="text-decoration-none">{{ $post->author->name }}</a>
                    dari kaetegori <a href="/posts?category={{ $post->category->slug }}" class="text-decoration-none">{{ $post->category->nama }}</a>
                </p>
                <h5>{{ $post["surah"] }}</h5>

                @if ($post->image)
                    <img src="{{ asset('storage/' . $post->image) }}" alt="..." class="img-fluid">
                @else
                    <img src="{{ url('Hoki128.png') }}" alt="..." class="img-fluid">
                @endif

                <article class="my-4 fs-5">
                {!! $post->isi !!}
                </article>

                <a href="/posts" class="d-block mt-3 mb-5">Back to Post</a>
            </div>
        </div>
    </div>

@endsection