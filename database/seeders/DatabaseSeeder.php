<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    
        User::create([
           'name' => 'Fadhilah Asri',
           'username' => 'Asriii',
           'email' => 'fadilahasri@gmail.com',
           'password' => bcrypt('hasan')
        ]);

        \App\Models\User::factory(2)->create();

        Category::create([
            'nama' => 'Programming',
            'slug' => 'programming'
        ]);

        Category::create([
            'nama' => 'Hardware',
            'slug' => 'hardware'
        ]);

        Category::create([
            'nama' => 'Design',
            'slug' => 'design'
        ]);

        Post::factory(10)->create();

        // Post::create([
        //     'judul' => 'Laravel',
        //     'slug' => 'laravel',
        //     'excrept' => 'Laravel is a web application framework with expressive, elegant syntax. Wev have already laid the foundation — freeing you to create without sweating the small things.',
        //     'isi'=> '<p>Laravel is a web application framework with expressive, elegant syntax. 
        //              A web framework provides a structure and starting point for creating your application, 
        //              allowing you to focus on creating something amazing while we sweat the details.</p><p>Laravel strives to provide an amazing developer experience while providing 
        //              powerful features such as thorough dependency injection, an expressive database abstraction layer, queues and scheduled jobs, unit and integration testing, 
        //              and more.</p>',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);

        // Post::create([
        //     'judul' => 'Boostrap',
        //     'slug' => 'boostrap',
        //     'excrept' => 'Powerful, extensible, and feature-packed frontend toolkit. Build and customize with Sass, utilize prebuilt grid system and components, and bring projects to life with powerful JavaScript plugins.',
        //     'isi'=> '<p>Laravel is a web application framework with expressive, elegant syntax. 
        //              A web framework provides a structure and starting point for creating your application, 
        //              allowing you to focus on creating something amazing while we sweat the details.</p><p>Laravel strives to provide an amazing developer experience while providing 
        //              powerful features such as thorough dependency injection, an expressive database abstraction layer, queues and scheduled jobs, unit and integration testing, 
        //              and more.</p>',
        //     'category_id' => 1,
        //     'user_id' => 3
        // ]);

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
