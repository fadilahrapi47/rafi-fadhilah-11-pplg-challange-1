<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;

class PostControll extends Controller
{
    public function home()
    {
        return view('home', [
            'title' => 'Home',
            'active' => 'home'
        ]);
    }

    public function category()
    {
            return view('categories', [
                'title' => 'Post Categories',
                'active' => 'post categories',
                'categories' => Category::all()
            ]);
    }
    
    public function index() 
    {
        $title = '';
        if(request('category')) {
            $category = Category::firstWhere('slug', request('category'));
            $title = ' di ' . $category->nama;       
        }
        if(request('author')) {
            $author = User::firstWhere('username', request('author'));
            $title = ' oleh ' . $author->name; 
        }
   
        return view('posts', [
            "title" => "Semua Postingan" . $title,
            "active" => "semua postingan",
            "posts" => Post::latest()->filter(request(['search', 'category', 'author']))->paginate(4)->withQueryString()
        ]);
    }
    
    public function show(Post $post) 
    {
        return view('post', [
            "title" => "Postingan",
            "active" => "semua postingan",
            "post" => $post
        ]);
    }

    public function about() 
    {
        return view('about', [
            "title" => "About",
            "active" => "about",
            "about" => "apa itu Hlover blog ?",
            'image' => 'Halal.png'
        ]);
    }
}
